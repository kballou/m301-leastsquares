function f, A, b = leastsquares(x, y)
    assert(length(x) == length(y), 'x length does not agree with y length');
    n = length(x);
    xbar = sum(x)/n;
    ybar = sum(y)/n;
    A = sum((x - xbar) .* (y - ybar)) / sum((y - ybar) .^ 2);
    b = ybar - xbar * A;
    f = @(x)(A*x + b);
end
