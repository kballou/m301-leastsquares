function project3()
    format long;
    stderr = @(x, y, f)(sqrt(1 / length(x)*sum(power(f(x) - y, 2))));
    % First Data Set
    Ax = [-2 -1 0 1 2];
    Ay = [1 2 3 3 4];
    [Alr, Am, Ab] = leastsquares(Ax, Ay);
    Ax_fit = linspace(-2, 2, 10);
    Ay_fit  = Alr(Ax_fit);
    A_stderr = stderr(Ax, Ay, Alr);

    % Second Data Set
    Bx = [-4 -2 0 2 4];
    By = [1.2 2.8 6.2 7.8 13.2];
    [Blr, Bm, Bb] = leastsquares(Bx, By);
    Bx_fit = linspace(-4, 4, 10);
    By_fit  = Blr(Bx_fit);
    B_stderr = stderr(Bx, By, Blr);

    %Output
    fprintf('Line for Data Set (1): y = %f(x) + %f\n', Am, Ab);
    fprintf('Data Set (1) standard error: %f\n', A_stderr);
    fprintf('Line for Data Set (2): y = %f(x) + %f\n', Bm, Bb);
    fprintf('Data Set (2) standard error: %f\n', B_stderr);
    fig = figure('visible', 'off');
    plot(Ax, Ay, 'ro', Ax_fit, Ay_fit, 'b-');
    saveas(fig, 'plot1.png');
    plot(Bx, By, 'ro', Bx_fit, By_fit, 'b-');
    saveas(fig, 'plot2.png');
end
