# README #

Write a MATLAB function that finds the least squares line `y = Ax+B` for
`n` data points:

    { (x_k, y_k)}_{k=1}^{n},

where `n` is any positive integer, and compute the error:

    E = (1/n \sum_{k=1}^{n}(Ax_k+B-y_k)^2)^(1/2)

Apply your function to the data:

1)

    x_k: -2, -1, 0, 1, 2,
    y_k:  1, 2, 3, 3, 4,

2)

    x_k: -4, -2, 0, 2, 4
    y_k: 1.2, 2.8, 6.2, 7.8, 13.2
